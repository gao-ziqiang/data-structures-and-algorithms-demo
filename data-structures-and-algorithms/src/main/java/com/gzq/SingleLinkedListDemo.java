package com.gzq;

/**
 * @author GaoZQ
 * @date 2021/12/21 22:59
 * @描述
 */
public class SingleLinkedListDemo {

}

/**
 * 模拟单链表 -- 封装一些方法
 */
class SingleLinkedList {

    /**
     * 模拟一个带头节点的链表，这里头节点为空
     */
    private Node head;



}

/**
 * 节点类 -- 模拟链表中的节点  包含data和next
 */
class Node {

    /**
     * 节点的数值
     */
    private int data;

    /**
     * 节点的下一个节点指向
     */
    private Node next;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", next=" + next +
                '}';
    }
}
